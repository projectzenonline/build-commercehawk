; Commerce Hawk

core = 7.x
api = 2

projects[drupal][type] = core
projects[drupal][version] = 7.23
projects[drupal][patch][1824820] = "https://drupal.org/files/string-offset-cast-1824820-2.patch"
projects[drupal][patch][1275902] = "https://drupal.org/files/1275902-15-entity_uri_callback-D7.patch"

projects[commerce_kickstart][type] = profile
projects[commerce_kickstart][download][type] = git
projects[commerce_kickstart][download][url] = http://git.drupal.org/project/commerce_kickstart.git
projects[commerce_kickstart][download][branch] = 7.x-2.x

projects[commerce_kickstart][patch][commercehawk] = https://bitbucket.org/commercehawk/build-commercehawk/raw/master/commerce_kickstart.patch

projects[commercehawk][type] = module
projects[commercehawk][download][type] = git
projects[commercehawk][download][url] = ssh://git@bitbucket.org/commercehawk/commercehawk.git

projects[commerce_fedex][version] = 1.x-dev
projects[commerce_stripe][version] = 1.0-rc5

projects[features_extra][version] = 1.0-beta1
projects[features_roles_permissions][version] = 1.0

projects[panels][version] = 3.3

projects[views_slideshow][version] = 3.0
libraries[jquery.cycle][type] = library
libraries[jquery.cycle][download][type] = git
libraries[jquery.cycle][download][url] = "https://github.com/malsup/cycle.git"
libraries[jquery.cycle][directory_name] = "jquery.cycle"

libraries[json2][type] = library
libraries[json2][download][type] = git
libraries[json2][download][url] = "https://github.com/douglascrockford/JSON-js.git"
libraries[json2][directory_name] = "json2"

projects[commerce_hawk_demo_theme][type] = theme
projects[commerce_hawk_demo_theme][download][type] = git
projects[commerce_hawk_demo_theme][download][url] = "ssh://git@bitbucket.org/commercehawk/commerce-hawk-demo-theme.git"
projects[commerce_hawk_demo_theme][download][branch] = master
projects[commerce_hawk_demo_theme][directory_name] = "commerce_hawk_demo_theme"

projects[addtoany][version] = 4.4

projects[contact_forms][version] = 1.8

projects[media][version] = 1.3
projects[media_youtube][version] = 2.0-rc3
projects[emfield][version] = 1.0-alpha2
projects[field_group][version] = 1.2
projects[field_group][patch][2078201] = https://www.drupal.org/files/2078201-27-fieldgroup_notice_flood.patch
projects[ds][version] = 2.6

projects[galleria][type] = module
projects[galleria][download][type] = git
projects[galleria][download][url] = "http://git.drupal.org/project/galleria.git"
projects[galleria][download][branch] = 7.x-1.x
projects[galleria][patch][1923148] = https://drupal.org/files/galleria-json_slides-1923148-12.patch

libraries[galleria][type] = library
libraries[galleria][download][type] = get
libraries[galleria][download][url] = http://galleria.io/static/galleria-1.2.9.zip
libraries[galleria][directory_name] = "galleria"

projects[mailchimp][version] = 2.12
libraries[mailchimp][type] = library
libraries[mailchimp][download][type] = get
libraries[mailchimp][download][url] = http://apidocs.mailchimp.com/api/downloads/mailchimp-api-class.zip
libraries[mailchimp][directory_name] = "mailchimp"

projects[logintoboggan][version] = 1.3
projects[logintoboggan][patch][1138122] = "https://drupal.org/files/permission_neutral_profile_link-1138122-3.patch"

projects[feeds][version] = 2.0-alpha8
projects[commerce_feeds][version] = 1.3
projects[feeds_tamper][version] = 1.0-beta5
projects[job_scheduler][version] = 2.0-alpha3
