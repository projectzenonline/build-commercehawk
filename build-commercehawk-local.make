core = 7.x
api = 2


includes[build-commercehawk] = "build-commercehawk.make"

projects[commercehawk][download][url] = "file:///var/aegir/shared_platforms/commercehawk"
projects[commercehawk][download][branch] = "feature/misc-fixes"

projects[commerce_kickstart][patch][commercehawk] = "file:///var/aegir/shared_platforms/build-commercehawk/commerce_kickstart.patch"

